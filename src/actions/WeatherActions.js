import {
  FETCH_WEATHER_CONDITION,
  FETCH_WEATHER_CONDITION_SUCCESS,
  FETCH_WEATHER_CONDITION_ERROR,
  FETCH_WEATHER_FORECAST,
  FETCH_WEATHER_FORECAST_SUCCESS,
  FETCH_WEATHER_FORECAST_ERROR
} from "../actions/ActionTypes";

import { fetchCurrent, fetchForecast } from "../api/weather";

export const requestCondition = function(city, country = "au") {
  return (dispatch, getState) => {
    dispatch({ type: FETCH_WEATHER_CONDITION, payload: {} });
    return fetchCurrent(city, country).then(
      data =>
        dispatch({
          type: FETCH_WEATHER_CONDITION_SUCCESS,
          payload: { data, pending: false }
        }),
      error =>
        dispatch({ type: FETCH_WEATHER_CONDITION_ERROR, payload: { error } })
    );
  };
};

export const requestForecast = function(city, country = "au") {
  return (dispatch, getState) => {
    dispatch({ type: FETCH_WEATHER_FORECAST });
    return fetchForecast(city, country).then(
      data =>
        dispatch({
          type: FETCH_WEATHER_FORECAST_SUCCESS,
          payload: { data }
        }),
      error =>
        dispatch({ type: FETCH_WEATHER_FORECAST_ERROR, payload: { error } })
    );
  };
};

export const requestAll = function(city, country = "au") {
  return (dispatch, getState) => {
    dispatch(requestCondition(city));
    dispatch(requestForecast(city));
    // return Promise.all([fetchCurrent(city), fetchForecast(city)]).then(
    //   ([condition, forecast]) => {
    //     dispatch({
    //       type: FETCH_WEATHER_CONDITION_SUCCESS,
    //       payload: { data: condition }
    //     });
    //     dispatch({
    //       type: FETCH_WEATHER_FORECAST_SUCCESS,
    //       payload: { data: forecast }
    //     });
    //   }
    // );
  };
};
