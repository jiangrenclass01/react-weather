import React, { Component } from "react";

// Presentational components for the left and right part of weather display
// WeatherChannel serves container component to pass data state via props needed by
// those presentational components
import CityCondition from "./CityCondition";
import Forecast from "./Forecast";

import Toolbar from "./Toolbar";

import { fetchConditionData, fetchForecast } from "../api/weather";

export default class WeatherChannel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curCity: "melbourne",
      unit: "C",
      autoRefresh: false,
      conditionData: {},
      forecastData: []
    };
  }

  componentDidMount() {
    const { curCity } = this.state;
    fetchConditionData(curCity)
      .then(data => this.onCityConditionLoad(data))
      .catch(error => {
        alert(error.message);
      });

    fetchForecast(curCity).then(data => {
      // console.log(forecast);
      this.onForecastLoad(data);
    });
  }

  // The next two methods used as callback when response come back from api
  // it extract data that presentational component need from response
  // setState with needed data to trigger re-rendering
  onCityConditionLoad(data) {
    const conditionData = {
      city: data.display_location.full,
      weather: data.weather,
      temp: { C: data.temp_c, F: data.temp_f },
      icon: data.icon,
      humidity: data.relative_humidity,
      wind: data.wind_kph,
      winddir: data.wind_dir
    };
    this.setState({ conditionData });
  }
  onForecastLoad(data) {
    const forecastData = data.map(item => {
      return {
        key: item.date.epoch,
        weekday: item.date.weekday_short,
        high: { C: item.high.celsius, F: item.high.fahrenheit },
        low: { C: item.low.celsius, F: item.low.fahrenheit },
        icon_url: item.icon_url,
        icon: item.icon
      };
    });
    this.setState({ forecastData });
  }

  selectCondition(data) {
    if (!Object.keys(data).length) return {};
    return {
      city: `${data.city.name} ${data.city.country}`,
      temp: { C: data.current.maxCelsius, F: data.current.maxFahrenheit },
      humidity: data.current.humidity,
      windSpeed: data.current.windSpeed,
      windDirection: data.current.windDirection
    };
  }

  selectForecast(data) {
    return data.map(item => {
      const date = new Date(item.time * 1000);
      return {
        key: item.time,
        weekday: date.toDateString().split(" ")[0],
        time: date.toLocaleTimeString(),
        low: { C: item.minCelsius, F: item.minFahrenheit },
        high: { C: item.maxCelsius, F: item.maxFahrenheit }
      };
    });
  }

  onCitySubmit() {
    const { curCity } = this.state;
    fetchConditionData(curCity)
      .then(data => this.onCityConditionLoad(data))
      .catch(error => {
        alert(error.message);
      });

    fetchForecast(curCity)
      .then(data => {
        this.onForecastLoad(data);
      })
      .catch(error => alert(error.message));
  }

  render() {
    // destruct ui state (curCity we search and temp unit we selected)
    // those are state to control Toolbar, see below how we pass via props
    const { curCity, unit, numOfDays, autoRefresh } = this.state;

    // destruct data state to be used in left and right presentational components
    // Look how nested object (conditionData) get destructed
    const { conditionData, forecastData } = this.state;
    return (
      <main>
        <Toolbar
          curCity={curCity}
          unit={unit}
          autoRefresh={autoRefresh}
          onAutoRefresh={() => this.setState({ autoRefresh: !autoRefresh })}
          onCityChange={e => this.setState({ curCity: e.target.value })}
          onCitySubmit={() => this.onCitySubmit()}
          onUnitChange={unit => {
            this.setState({ unit });
          }}
        />
        <section className="weather-condition">
          {/* city={city} weather={weather} temp={temp} unit={unit} */}
          <CityCondition data={conditionData} unit={unit} />
        </section>
        <section className="weather-forecast">
          <Forecast days={forecastData} unit={unit} />
        </section>
      </main>
    );
  }
}
