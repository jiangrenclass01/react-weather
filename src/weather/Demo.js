import React from 'react';

import CityConditon from './CityConditon';
import Forecast from './Forecast';

import {fetchConditionData, fetchForecast} from './api/weather';


class WeatherChannel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      curCity: 'brisbane',
      condition: {},
      days: [],
    };
  }

  componentDidMount() {
    store.subscribe(() => {
      const {a, b} = store.getState()
      this.setState({a, b})
   })
  }
   
   handleSomeEvent() {
      store.dispatch({
          type: ''
          // any data object as payload
      })
   }

   componentWillUnmount() {
     store.unsubsribe();
   }

  handleCityChange(event) {
    const value = event.target.value;
    this.setState({curCity: value});
  }

  onConditionLoad(data) {
    const condition = {
      city: data.display_location.full,
      temp: {C:data.temp_c, F:data.temp_f},
      weather: data.weather
    }
    this.setState({condition});
  }
  handleSearch() {
    const city = this.state.curCity;
    fetchConditionData(city).then(data => {
      this.onConditionLoad(data);
    });
    fetchForecast(city).then(data => {
      this.onForecastLoad()
    })
  }
  
  render() {
    return (
      <React.Fragment>
      <nav>
      <div style={{flex:1}}>
        <input className="search-input" value={this.state.curCity} onChange={this.handleCityChange.bind(this)} />
        <button className="search-btn" onClick={this.handleSearch.bind(this)}><i class="fa fa-search"></i></button>
        </div>
        </nav>
      <main>
        <section className="weather-conditio">
          <CityConditon data={this.state.condition} />
        </section>
        <section className="weather-forecast">
          <Forecast days={this.state.days} />
        </section>
      </main>
      </React.Fragment>
    );
  }
}

function Forecast(props) {
  const days = props.days;
  let rows = [];
  // for (let i = 0; i < days.length; i++) {
  //   let day = days[i];
  //   rows.push(row);
  // }
  rows = days.map(day => {
    return (
      <div key={} className="weather-forecast__row">
        <span className="weather-forecast__day">{day.weekday}</span>
        <span class="weather-forecast__icon">
          <img src={day.icon} />
        </span>
        <span class="weather-forecast__high">{day.high}</span>
        <span class="weather-forecast__low">{day.low}</span>
      </div>
    );
  });
  return <div>{rows}</div>;

  days.map((day, index) => {return <Row key={`${day.weekday}_${index}`} day={day} />})
}
function DailyItem(props) {
  const day = props.day;
  return (
    <div class="weather-forecast__row">
      <span class="weather-forecast__day">{day.weekday}</span>
      <span class="weather-forecast__icon">
        <img src={day.icon} />
      </span>
      <span class="weather-forecast__high">{day.high}</span>
      <span class="weather-forecast__low">{day.low}</span>
    </div>
  );
}


// weather.js
import axios from 'axios';


const CONDITION_BASE_URL =
  'http://api.wunderground.com/api/f029e46fd0232d12/geolookup/conditions/q/Australia/';
const FORECAST_BASE_URL =
  'http://api.wunderground.com/api/f029e46fd0232d12/geolookup/forecast10day/q/Australia/';

export function fetchConditionData(city) {
  const url = `${CONDITION_BASE_URL}${city}.json`;
  return axios.get(url).then(response => response.data.current_observation);
}

// src/Course/CoursesView
import React, {Component} from 'react';

import {fetchCourses} from '../api/course';
export default class CoursesView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      courses: []
    }
  }

  componentDidMount() {
    // trigger initial request when page is visited
    fetchCourses().then(data => {

      this.setState({courses: data})
    })
  }

  render() {
    return (
      this.state.courses
      .map(course => <h1>{course.Name}</h1>)
    )
  }
}

// src/api/course.js
import axios from 'axios';

export function fetchCourses() {
  return axios.get('/course').then(response => {
    // further process response.data
    return response.data;
  })
  // if only resolve as response.data
  // return axios.get('/course').then(response => response.data)
}

export function fetchCourseById(id) {
  //
}

// in CourseEditView.js
import React, {Component} from 'react';

import {fetchCourseById, 
  createCourse, 
  updateCourse} from '../api/course';

export default class CourseEditView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      course: {}
    }
  }

  componentDidMount() {
    const id = this.props.match.params.id;
    if (id === 'NEW') return;

    fetchCourseById(id).then(data => {
      this.setState({course: data});
    })
  }

  handleInputChange = (event) => {
    const {name: fieldName, value} = event.target;
    const course = this.state;
    const nextCourse = {
      name: course.name,
      code: course.code
    };
    nextCourse[fieldName] = value;
    this.setState({
      course: nextCourse
    })
  }

  handleSubmit = () => {
    const id = this.props.match.params.id;
    const course = this.state.course;
    if (id === 'NEW') {
      createCourse(course);
    } else {
      updateCourse(id, course);
    }
  }

  render() {
    const course = this.state.course;
    return (
      <form onSubmit={this.handleSubmit} >
        <div className='form-group'>
          <label>Course code</label>
          <input 
            className='form-control' 
            name='code' 
            value={course.code || ''} 
            onChange={this.handleInputChange} />
        </div>

        <div className='form-group'>
          <label>Course name</label>
          <input 
            className='form-control' 
            name='name' 
            value={course.name || ''} 
            onChange={this.handleInputChange} />
        </div>



      </form>
    )
  }
}
