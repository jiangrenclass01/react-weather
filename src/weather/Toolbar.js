import React, { Component } from "react";
import { connect } from "react-redux";

import { CHANGE_CITY, CHANGE_TEMP_UNIT } from "../actions/ActionTypes";
import { requestAll } from "../actions/WeatherActions";

class AutoRefreshSwitch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reloading: false,
      switchOn: false,
      countDown: props.interval
      // counting
    };
  }

  reload() {
    this.setState({ reloading: true });
    clearInterval(this._reloadTimer);
    clearInterval(this._countTimer);
    this.props.onCitySubmit();
    // .then(() => {
    //   this.setState({countDown: 60});
    //   this._countTimer = setInterval(() => {
    //     this.setState({ countDown: this.state.countDown - 1 });
    //   }, 1000);

    // })
  }

  onSwitch() {
    this.setState(prevState => {
      return {
        switchOn: !prevState.switchOn
      };
    });
    this._reloadTimer = setInterval(
      this.props.onTimerEnd,
      this.props.interval * 1000
    );
    this._countTimer = setInterval(() => {
      this.setState({ countDown: this.state.countDown - 1 });
    }, 1000);
  }

  render() {
    const { switchOn } = this.state;
    return (
      <div style={{ display: "flex", alignItems: "center" }}>
        <span style={{ marginRight: 15 }}>{`${this.state.countDown} s`}</span>
        <div className="onoffswitch">
          <input
            checked={switchOn}
            type="checkbox"
            name="onoffswitch"
            className="onoffswitch-checkbox"
            id="myonoffswitch"
            onChange={() => this.onSwitch()}
          />
          <label className="onoffswitch-label" htmlFor="myonoffswitch">
            <span className="onoffswitch-inner" />
            <span className="onoffswitch-switch" />
          </label>
        </div>
      </div>
    );
  }
}
AutoRefreshSwitch.defaultProps = { interval: 10 };

class ToolbarX extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curCity: props.curCity
    };
  }

  render() {
    const { curCity, unit } = this.props;
    return (
      <nav>
        <div style={{ flex: 1 }}>
          <input
            className="search-input"
            value={this.state.curCity}
            onChange={e => this.setState({ curCity: e.target.value })}
          />
          <button
            className="search-btn"
            onClick={() => this.props.changeCity(this.state.curCity)}
          >
            <i className="fa fa-search" />
          </button>
          <button
            className="temp-switch"
            onClick={() => {
              unit === "C"
                ? this.props.switchTempUnit("F")
                : this.props.switchTempUnit("C");
            }}
          >
            <i className="fa fa-thermometer-empty" style={{ marginRight: 8 }} />
            {unit.toUpperCase()}
          </button>
        </div>
      </nav>
    );
  }
}

function ToolbarY(props) {
  const { curCity, unit } = props;
  return (
    <nav>
      <div style={{ flex: 1 }}>
        <input
          className="search-input"
          value={curCity}
          onChange={e => props.changeCity(e.target.value)}
        />
        <button
          className="search-btn"
          onClick={() => props.requestAll(curCity)}
        >
          <i className="fa fa-search" />
        </button>
        <button
          className="temp-switch"
          onClick={() => {
            unit === "C"
              ? props.switchTempUnit("F")
              : props.switchTempUnit("C");
          }}
        >
          <i className="fa fa-thermometer-empty" style={{ marginRight: 8 }} />
          <sup>&deg;</sup> {unit.toUpperCase()}
        </button>
        {/* <i className="fa fa-spinner fa-spin fa-fw" style={{ marginLeft: 20 }} /> */}
      </div>
    </nav>
  );
}

const mapStateToProps = state => ({
  pending: state.weatherData.pending
});
const mapDispatchToProps = dispatch => ({
  changeCity: city => dispatch({ type: CHANGE_CITY, curCity: city }),
  switchTempUnit: unit => dispatch({ type: CHANGE_TEMP_UNIT, unit }),
  requestAll: city => dispatch(requestAll(city))
});

export default connect(
  null,
  mapDispatchToProps
)(ToolbarY);
