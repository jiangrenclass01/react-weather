import React from "react";

import iumberella from "../images/icons/icon-umberella.png";
import iwind from "../images/icons/icon-wind.png";
import icompass from "../images/icons/icon-compass.png";

const PUBLIC_URL = process.env.PUBLIC_URL;

export default function CityCondition(props) {
  const {
    city,
    weather,
    temp,
    humidity,
    windSpeed,
    windDirection
  } = props.data;
  const unit = props.unit;
  // console.log("render condition");
  return (
    <div>
      <div className="weather-condition__location">{city}</div>
      <div style={{ textAlign: "center", fontSize: 14 }}>{weather}</div>
      <div className="weather-condition__temp">
        <span style={{ paddingRight: 18 }}>
          {temp[unit]}
          <sup style={{ paddingTop: 50, paddingLeft: 15 }}>&deg;</sup>
          {unit.toLowerCase()}
          {/* {`${temp[unit]} ${unit.toLowerCase()}`} */}
        </span>
        {/* <img style={{ width: 88, height: 60 }} src={`${PUBLIC_URL}/icons/${icon}.svg`} /> */}
      </div>
      <div className="weather-condition__desc">
        <div>
          <img src={iumberella} /> <span className="citem">{humidity}</span>
        </div>
        <div>
          <img src={iwind} />{" "}
          <span className="citem">{`${windSpeed} km/h`}</span>
        </div>
        <div>
          <img src={icompass} /> <span className="citem">{windDirection}</span>
        </div>
      </div>
    </div>
  );
}
