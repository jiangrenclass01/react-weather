import React, { Component } from "react";
import { connect } from "react-redux";

import CityCondition from "./CityCondition";
import Forecast from "./Forecast";
import Toolbar from "./Toolbar";

// import { fetchCurrent, fetchForecast } from "../api/weather";
import { requestCondition, requestForecast } from "../actions/WeatherActions";
import { timingSafeEqual } from "crypto";

class WeatherChannel extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.requestCondition(this.props.curCity);
    this.props.requestForecast(this.props.curCity);
  }

  // componentDidUpdate(prevProps, prevState) {
  //   // const prevCity = prevProps.curCity;
  //   // const nextCity = this.props.curCity;
  //   // if (prevCity != nextCity) {
  //   //   this.props.requestCondition(this.props.curCity);
  //   //   this.props.requestForecast(this.props.curCity);
  //   // }
  // }

  renderSpinner() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          minHeight: 200
        }}
      >
        <i
          className="fa fa-spinner fa-3x fa-spin fa-fw"
          // style={{ marginTop: 20, marginBottom: 20 }}
        />
      </div>
    );
  }

  renderError() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          minHeight: 200
        }}
      >
        <span>☹️ Fail to fetch</span>
      </div>
    );
  }
  renderCondition(condition, tempUnit) {
    let content = null;
    if (condition.pending) {
      return this.renderSpinner();
    }
    if (condition.error) {
      // alert("Error");
      return this.renderError();
    }
    if (condition.data) {
      return <CityCondition data={condition.data} unit={tempUnit} />;
    }
  }

  renderForecast(forecast, tempUnit) {
    if (forecast.pending) {
      return this.renderSpinner();
    }
    if (forecast.error) {
      // return
    }
    if (forecast.data) {
      return <Forecast data={forecast.data} unit={tempUnit} />;
    }
  }

  render() {
    const { condition, forecast, curCity, tempUnit } = this.props;
    console.log(`[WeatherChannel.render]`);
    return (
      <main>
        <Toolbar curCity={curCity} unit={tempUnit} />
        <section className="weather-condition">
          {this.renderCondition(condition, tempUnit)}
        </section>
        <section className="weather-forecast">
          {this.renderForecast(forecast, tempUnit)}
        </section>
      </main>
    );
  }
}

const mapStateToProps = state => {
  return {
    condition: state.weatherData.condition,
    forecast: state.weatherData.forecast,
    curCity: state.options.curCity,
    tempUnit: state.options.tempUnit
  };
};

const mapDispatchToProps = dispatch => ({
  requestCondition: city => dispatch(requestCondition(city)),
  requestForecast: city => dispatch(requestForecast(city))
});

export default connect(
  // speficy what data needed from state
  mapStateToProps,
  // speficy what actions needed to dispatch
  mapDispatchToProps
)(WeatherChannel);
