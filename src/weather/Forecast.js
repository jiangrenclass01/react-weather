import React from "react";
import { ReactReduxContext } from "react-redux";

// const PUBLIC_URL = process.env.PUBLIC_URL;
// const mapping = {
//   partlycloudy: "partlycloudy.svg",
//   mostlycloudy: "mostlycloudy.svg",
//   cloudy: "cloudy.svg",
//   clear: "clear.svg"
// };

function DailyItem({ day, unit }) {
  {
    /* <span className="weather-forecast__icon">
        <img src={`${PUBLIC_URL}/icons/${day.icon}.svg`} />
      </span> */
  }
  return (
    <ReactReduxContext.Consumer>
      {value => {
        // console.log(value);
        return (
          <div className="weather-forecast__row">
            <span className="weather-forecast__day">{`${day.weekday}`}</span>
            <span className="weather-forecast__icon">
              <i className="fa fa-clock-o" style={{ paddingRight: 5 }} />
              {day.time}
            </span>

            <span className="weather-forecast__high">{day.high[unit]}</span>
            <span className="weather-forecast__low">{day.low[unit]}</span>
          </div>
        );
      }}
    </ReactReduxContext.Consumer>
  );
}

export default class Forecast extends React.Component {
  constructor(props) {
    super(props);
    this.state = { numOfItems: 5 };
  }
  render() {
    const { data, unit } = this.props;
    const { numOfItems } = this.state;
    console.log("render forecast");
    let btnClass0 =
      numOfItems == 5
        ? ["forecast__switch_0 switch-active"].join(" ")
        : "forecast__switch_0";
    let btnClass1 =
      numOfItems == 10
        ? ["forecast__switch_1 switch-active"].join(" ")
        : "forecast__switch_1";
    return (
      <div>
        <div className="forecast__switch">
          <button
            className={btnClass0}
            onClick={e => this.setState({ numOfItems: 5 })}
          >
            5 items
          </button>
          <button
            className={btnClass1}
            onClick={e => this.setState({ numOfItems: 10 })}
          >
            10 items
          </button>
        </div>
        {data.slice(0, numOfItems).map((item, i) => (
          <DailyItem key={item.key} day={item} unit={unit} />
        ))}
      </div>
    );
  }
}
