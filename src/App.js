import React, { Component } from "react";
import "./styles/main.css";

import Header from "./weather/Header";
// import CityCondition from './weather/CityCondition';
import Footer from "./weather/Footer";
import WeatherChannel from "./weather/WeatherChannel";

// const conditionData = {
//   city: 'Brisbane, Australia',
//   weather: 'clear',
//   temp: 19,
// };
export default class App extends Component {
  render() {
    return (
      <div className="weather-channel__container">
        <Header />
        <WeatherChannel />
        <Footer />
      </div>
    );
  }
}
