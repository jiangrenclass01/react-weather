import {
  FETCH_WEATHER_CONDITION,
  FETCH_WEATHER_CONDITION_SUCCESS,
  FETCH_WEATHER_CONDITION_ERROR,
  FETCH_WEATHER_FORECAST,
  FETCH_WEATHER_FORECAST_SUCCESS,
  FETCH_WEATHER_FORECAST_ERROR
} from "../actions/ActionTypes";

const initialState = {
  condition: { pending: false, error: null },
  forecast: { pending: false, error: null }
};

const extractCondition = data => ({
  city: `${data.city.name}, ${data.city.country}`,
  temp: { C: data.current.maxCelsius, F: data.current.maxFahrenheit },
  humidity: data.current.humidity,
  windSpeed: data.current.windSpeed,
  windDirection: data.current.windDirection
});
const extractForecast = data => {};

const weatherReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case FETCH_WEATHER_CONDITION:
      return { ...state, condition: { pending: true, error: null } };
    case FETCH_WEATHER_CONDITION_SUCCESS:
      let data = payload.data;
      return {
        ...state,
        condition: {
          pending: false,
          data: {
            city: `${data.city.name}, ${data.city.country}`,
            temp: { C: data.current.maxCelsius, F: data.current.maxFahrenheit },
            humidity: data.current.humidity,
            windSpeed: data.current.windSpeed,
            windDirection: data.current.windDirection
          }
        }
      };
    case FETCH_WEATHER_CONDITION_ERROR:
      return {
        ...state,
        condition: { pending: false, error: payload.error }
      };

    case FETCH_WEATHER_FORECAST:
      return { ...state, forecast: { pending: true, error: null } };
    case FETCH_WEATHER_FORECAST_SUCCESS:
      return {
        ...state,
        forecast: {
          pending: false,
          data: payload.data.map(item => {
            const date = new Date(item.time * 1000);
            return {
              key: item.time,
              weekday: date.toDateString().split(" ")[0],
              time: date.toLocaleTimeString().substr(0, 5),
              low: { C: item.minCelsius, F: item.minFahrenheit },
              high: { C: item.maxCelsius, F: item.maxFahrenheit }
            };
          })
        }
      };
    case FETCH_WEATHER_FORECAST_ERROR:
      return { ...state, forecast: { pending: false, error: payload.error } };
    default:
      return state;
  }
};
export default weatherReducer;
