import { combineReducers } from "redux";

import weatherReducer from "./weatherReducer";
import optionsReducer from "./optionsReducer";

// State tree design
// const state = {
// 	weatherData: {
// 		condition, forecast,
// 	},
// 	options: {
// 		curCity: '',
// 		tempUnit: 'C', numOfItems: 5
// 	}
// }

export default combineReducers({
  weatherData: weatherReducer,
  options: optionsReducer
});
