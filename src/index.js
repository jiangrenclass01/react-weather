import React from "react";
import ReactDOM from "react-dom";
// import App from './App';
import AppContainer from "./AppContainer";

ReactDOM.render(<AppContainer />, document.getElementById("root"));
