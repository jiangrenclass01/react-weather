import React, { Component } from "react";
import { Provider } from "react-redux";

import store from "./store";
import Header from "./weather/Header";
import Footer from "./weather/Footer";
import WeatherChannel from "./weather/WeatherChannel";

import "./styles/main.css";

export default class AppContainer extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="weather-channel__container">
          <Header />
          <WeatherChannel />
          <Footer />
        </div>
      </Provider>
    );
  }
}
