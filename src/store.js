import { createStore, applyMiddleware } from "redux";
// import {logger} from 'redux-logger'
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import reducers from "./reducers";
const middlewares = [thunk];
// if (process.env.NODE_ENV !== "production") {
//   middlewares.push(logger);
// }

export default createStore(
  reducers,
  composeWithDevTools(applyMiddleware(thunk))
);
